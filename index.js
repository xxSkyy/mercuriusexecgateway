const { loadSchema } = require("@graphql-tools/load")
const {
  addResolversToSchema,
  makeExecutableSchema,
} = require("@graphql-tools/schema")
const { GraphQLFileLoader } = require("@graphql-tools/graphql-file-loader")

const fs = require("fs")

exports.makeGatewayExecutableSchema = async ({
  schemaPath,
  schemaString,
  resolvers,
}) => {
  const serviceInfo = `
  type Service {
    sdl: String
  }

  type Query {
    _service: Service
  }
  type Mutation {
    _service: Service
  }
  type Subscription {
    _service: Service
  }

  extend type`

  let schema

  if (schemaPath) {
    try {
      schema = await fs.readFileSync(schemaPath, "utf-8")
    } catch (e) {
      console.log(e)
      throw Error("Can't load schema file")
    }
  } else {
    schema = schemaString
  }

  if (
    schema.indexOf("extend type Query") === -1 &&
    schema.indexOf("type Query") !== -1
  ) {
    throw Error(
      `You can't have 'type Query' in your schema, 
      only 'extend type Query' and so on with mutations and subscriptions`
    )
  }

  // Create temp file that gql-tools will load - mandatory for prossibly
  // future functionality of imports
  // let tempFilePath
  // if (schemaPath) {
  //   const isGql = schemaPath.indexOf(".gql") !== -1
  //   const isGraphql = schemaPath.indexOf(".graphql") !== -1
  //   if (!isGql && !isGraphql)
  //     throw Error("Schema file must end on .gql or .graphql")

  //   if (isGql) tempFilePath = schemaPath.replace(".gql", "_temp.gql")

  //   if (isGraphql)
  //     tempFilePath = schemaPath.replace(".graphql", "_temp.graphql")
  // } else {
  //   tempFilePath = "./schema_temp.gql"
  // }
  const tempSchema = schema.replace("extend type", serviceInfo)
  // await fs.writeFileSync(tempFilePath, tempSchema)
  //

  // const schemaFiles = await loadSchema(tempSchema)
  // Load and delete temp file
  // const schemaFiles = await loadSchema(tempFilePath, {
  //   loaders: [new GraphQLFileLoader()],
  // })
  // await fs.unlinkSync(tempFilePath)

  // Add resolver
  resolvers.Query._service = () => {
    return { sdl: `${schema}` }
  }

  // return addResolversToSchema({
  //   schema: schemaFiles,
  //   resolvers,
  // })

  return makeExecutableSchema({
    typeDefs: tempSchema,
    resolvers,
  })
}

const replaceUselessCode = (schema) =>
  schema
    .replace("type Service {\n  sdl: String\n}\n\n", "")
    .replace("extend type Query {\n  }\n\n", "")

exports.makeGatewayExecutableSchema = async ({
  schemaPath,
  schemaString,
  resolvers,
}) => {
  const serviceInfo = `
  type Service {
    sdl: String
  }

  type Query {
    _service: Service
  }`

  let schema

  if (schemaPath) {
    try {
      schema = await fs.readFileSync(schemaPath, "utf-8")
    } catch (e) {
      console.log(e)
      throw Error("Can't load schema file")
    }
  } else {
    schema = schemaString
  }
  let schemaArray = schema.split("\n")
  // First just replace query to extend, later it will be processed after making executable schema
  for (let i = 0; i < schemaArray.length; i++) {
    let line = schemaArray[i]
    line = line.replace(/\s+/g, " ").trim()
    if (
      line.indexOf("type Query") !== -1 &&
      line.indexOf("extend type Query") === -1
    )
      schemaArray[i] = line.replace("type", "extend type")
  }
  const convertedSchema = schemaArray.join("\n")

  // if (
  //   schema.indexOf("extend type Query") === -1 &&
  //   schema.indexOf("type Query") !== -1
  // ) {
  //   throw Error(
  //     `You can't have 'type Query' in your schema,
  //     only 'extend type Query' and so on with mutations and subscriptions`
  //   )
  // }

  // Create temp file that gql-tools will load - mandatory for prossibly
  // future functionality of imports
  // let tempFilePath
  // if (schemaPath) {
  //   const isGql = schemaPath.indexOf(".gql") !== -1
  //   const isGraphql = schemaPath.indexOf(".graphql") !== -1
  //   if (!isGql && !isGraphql)
  //     throw Error("Schema file must end on .gql or .graphql")

  //   if (isGql) tempFilePath = schemaPath.replace(".gql", "_temp.gql")

  //   if (isGraphql)
  //     tempFilePath = schemaPath.replace(".graphql", "_temp.graphql")
  // } else {
  //   tempFilePath = "./schema_temp.gql"
  // }
  // const tempSchema = schema.replace("type Query", serviceInfo)
  const tempSchema = `${serviceInfo}${convertedSchema}`
  // await fs.writeFileSync(tempFilePath, tempSchema)
  //

  // const schemaFiles = await loadSchema(tempSchema)
  // Load and delete temp file
  // const schemaFiles = await loadSchema(tempFilePath, {
  //   loaders: [new GraphQLFileLoader()],
  // })
  // await fs.unlinkSync(tempFilePath)

  const schemaToProcess = printSchema(
    makeExecutableSchema({
      typeDefs: tempSchema,
      resolvers,
    })
  )

  console.log(schemaToProcess)

  let processedSchema = schemaToProcess.replace("_service: Service\n", "")
  processedSchema = processedSchema.split("\n")
  for (let i = 0; i < processedSchema.length; i++) {
    let line = processedSchema[i]
    if (
      line.indexOf("type Query") !== -1 &&
      line.indexOf("extend type Query") === -1
    )
      processedSchema[i] = line.replace("type", "extend type")
    if (
      line.indexOf("type Mutation") !== -1 &&
      line.indexOf("extend type Mutation") === -1
    )
      processedSchema[i] = line.replace("type", "extend type")
    if (
      line.indexOf("type Subscription") !== -1 &&
      line.indexOf("extend type Subscription") === -1
    )
      processedSchema[i] = line.replace("type", "extend type")
  }

  processedSchema = processedSchema.join("\n")

  // Add resolver
  resolvers.Query = resolvers.Query || {}
  resolvers.Query._service = () => {
    return { sdl: `${replaceUselessCode(processedSchema)}` }
  }

  // return addResolversToSchema({
  //   schema: schemaFiles,
  //   resolvers,
  // })

  return makeExecutableSchema({
    typeDefs: tempSchema,
    resolvers,
  })
}
